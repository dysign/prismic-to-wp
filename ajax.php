<?php 

// --------------------
// 1. SET UP AND CHECKS
// --------------------

// Improve PHP 
ini_set('max_execution_time', 300);

// Load WP
define('WP_USE_THEMES', false);
require('../wp-load.php');

// Load dependencies for image
require_once( ABSPATH . 'wp-admin/includes/image.php' );

// Create special directory
define('UPLOAD_DIR', "../wp-content/uploads/import/");

if (!is_dir(UPLOAD_DIR)) { mkdir(UPLOAD_DIR, 0755); }

// Get json file
$file = file_get_contents("datas.json");
$json = json_decode( $file );

// JSON Error
switch (json_last_error()) {
  case JSON_ERROR_DEPTH:
    echo 'ERREUR JSON - Profondeur maximale atteinte';
  break;
  case JSON_ERROR_STATE_MISMATCH:
    echo 'ERREUR JSON - Inadéquation des modes ou underflow';
  break;
  case JSON_ERROR_CTRL_CHAR:
    echo 'ERREUR JSON - Erreur lors du contrôle des caractères';
  break;
  case JSON_ERROR_SYNTAX:
    echo 'ERREUR JSON - Erreur de syntaxe ; JSON malformé';
  break;
  case JSON_ERROR_UTF8:
    echo 'ERREUR JSON - Caractères UTF-8 malformés, probablement une erreur d\'encodage';
  break;
}

// Got from Js Ajax Loop
$occurence = $_POST['occurence'];

// Get only the current occurence
$entry = $json[$occurence];


// Do not import from specific category
$forbidden_cats = array('CAT 1', 'CAT 2', 'CAT 5');

if(isset($entry->tags[0]) and in_array($entry->tags[0], $forbidden_cats)):
  echo "Article <span style='color:lightsalmon'>ignoré (catégorie à exclure)</span> <em>$post_title</em><br>";
  die();
endif;

// Check if post doesn't already exists (check title)
$post_title = $entry->data->{'article.title'}->value[0]->text;

if(count(get_posts(array('title' => $post_title)))):
  echo "Article <span style='color:tomato'>existant</span> <em>$post_title</em><br>";
  die();
endif;


// --------------------
// 2. LET'S IMPORT
// --------------------


// Prepare post 
$datas = array(
  "post_status" => "draft",
  "post_title" => "PROV",
  "post_content" => ""
);
$post_id = wp_insert_post($datas);

// Set post Thumbnail
$post_thumbnail_url = $entry->data->{'article.image'}->value->main->url;
$thumbnail_id = import_pic($post_thumbnail_url, $post_id);  
set_post_thumbnail( $post_id, $thumbnail_id ); 

// Prepare date
$date = $entry->data->{'article.publish_date'}->value;
$date = str_replace("+0000", "", $date);
$date = str_replace("T", " ", $date);

// Prepare excerpt
$excerpt = (isset($entry->data->{'article.excerpt'}->value[0]->text)) ? $entry->data->{'article.excerpt'}->value[0]->text : "";


// Prepare content
$content = "";

// Sometimes article.text contains blocks, and sometims value
$article_obj = (isset($entry->data->{'article.text'}->blocks)) ? $entry->data->{'article.text'}->blocks : $entry->data->{'article.text'}->value ;

foreach($article_obj as $part):  
  //var_dump($part);

  if ($part->type == "paragraph"):

    $str = "";
    $last_end= 0;

    // handle formatting
    if(isset($part->spans)): foreach($part->spans as $span):

      // avoid double formating
      if($span->start >= $last_end):

        if($str == ""){
          $str = "<p>".mb_substr($part->text, 0, $span->start);
        } else {
          $str = $str.mb_substr($part->text, $last_end, $span->start);
        }

        $to_format = mb_substr($part->text, $span->start, $span->end - $span->start);

        if ($span->type == "strong") {
          $str = $str.'<strong>'.$to_format.'</strong>';
        }

        if ($span->type == "em") {
          $str = $str.'<em>'.$to_format.'</em>';
        }

        if ($span->type == "hyperlink") {
          $str = $str.'<a href="'.$span->data->value->url.'">'.$to_format.'</a>';
        }

        // for the last part 
        $last_end = $span->end;
      endif;

    endforeach; endif;

    // last part
    $content .= $str.mb_substr($part->text, $last_end, mb_strlen($part->text) - $last_end)."</p>";

  endif;

  if ($part->type == "embed"):
    $content .= '<p>'.$part->oembed->html.'</p>';
  endif;
  

  if ($part->type == "image" and isset($part->url)):
    $thumbnail_id = import_pic($part->url, $post_id);  
    $url = wp_get_attachment_image_src($thumbnail_id, 'large');
    $url = $url[0];

    $content.="<p class='center'><img src='".$url."' alt='".$part->alt."'></p>";
  endif;

endforeach;


// SET POST
$datas = array(
  "ID" => $post_id,
  "post_status" => "publish",
  "post_date" => $date,
  "post_date_gmt" => $date,
  "post_title" => $post_title,
  "post_name" => $entry->slug,
  "post_excerpt" => $excerpt,
  "post_content" => $content,
);

// Prepare categories
if(isset($entry->tags[0])){

  if($entry->tags[0] == "Cat 1"):
    $datas["post_category"] = array(1); // id de la catégorie

  elseif($entry->tags[0] == "Cat 2"):
    $datas["post_category"] = array(2); // id de la categorie
  endif;
  
}

// Insert Post
wp_update_post($datas);

// POST METAS
add_post_meta($post_id, "old_id", $entry->id, true);

// Print to Ajax
$title = $datas['post_title'];
echo "Article <span style='color:chartreuse'>inséré</span> [<strong>$post_id</strong>] \"<em>$title</em>\" <br>";



function import_pic($url, $parent_id) {

  $ext = pathinfo($url, PATHINFO_EXTENSION);
  $fullname = pathinfo($url, PATHINFO_FILENAME);
  $nicename = strstr($fullname, '_');
  $nicename = substr($nicename, 1);


  $file = UPLOAD_DIR.$nicename.".".$ext;

  file_put_contents($file, fopen($url, 'r'));

  $attachment = array(
    "post_title" => $nicename,
    "post_content" => "",
    "post_status" => "publish",
    "post_mime_type" => 'image/'.$ext
  );

  $attachment_id = wp_insert_attachment( $attachment, $file, $parent_id );

  $attachment_data = wp_generate_attachment_metadata( $attachment_id, $file );
  wp_update_attachment_metadata( $attachment_id,  $attachment_data );

  return $attachment_id;
}


die();
