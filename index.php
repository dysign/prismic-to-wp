<!DOCTYPE html>
<html lang="fr">
<head>
  <title>Import Prismic</title>
  <meta charset="utf-8">
  
  <style>
    body {
      background: black;
      color: white;
      padding: 20px;
      font-family: monospace;
    }

    button {
      background: tomato;
      color: white;
      border: none;
      border-radius: 5px;
      padding: 8px 15px;
      font-weight: bold;
      font-size: 16px;
    }
    button:hover {
      cursor: pointer;
    }

    .center {
      text-align: center;
    }
  </style>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script>
    $(document).ready(function() {

      var maxLoop = 655;
      var i = 0;
      var canLaunch = true

      function prismicImport() {
        canLaunch = false;
        
        $.ajax({
          type: "POST",
          url: "ajax.php",
          data: { occurence: i}                  
        }).always(function(msg) {
        
          i++;
          $('body').append(i+". "+msg);

          canLaunch = true;
        });
      }

      function launcher() {
        if (canLaunch && i < maxLoop){
          prismicImport();
        }
      }

      $('button').click(function(){
        $(this).hide();
        $('body').append("<p>c'est parti !</p>");
        setInterval(launcher, 100);
      });  

    });
  </script>
</head>  
<body>
  <p class="center"><button>Lancer l'import</button></p>
   
</body>
</html>